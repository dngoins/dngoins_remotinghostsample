# HololensDXTutorials
Hololens DirectX Tutorials in C# and C++

Examples include:
### C'#'
  **Texture Cube** - Draws a holographic cube with a picture/texture as opposed to just the gradient colors seen in the default UWP sample
	![Egiptian holding Candle](http://i.imgur.com/BhkN66c.jpg)
  
### C++
   **Recording Audio** - When you Air Tap, sounds from the Microphone are recorded and saved as a WAVE file format into the media folder. You can retrieve the file using the Microsoft HoloLens UWP application (can be downloaded from the Microsoft Store) or the Hololens internal Developer Web Portal.

  
  **Textured Spatial Mapping** - This examples shows how to make the Matrix effect or show different images laid out over your spatial mapping of a room.
![Matrix Spatial Mapping](http://i.imgur.com/R8pQWRe.jpg)

 **Marble** ![](http://i.imgur.com/QBMivlH.jpg)

**Artistic** ![Artistic](http://i.imgur.com/09xq0u4.jpg)
**Artistic 2** ![Artistic 2](http://i.imgur.com/hcE8gm4.jpg)

**Remoting Host Sample (DNGoins Mod)** - This example shows how to load multiple 3D Models in the Remoting Host Sample, and streams the models to the HoloLens. 
The example utilizes the Open Asset Importer open source library recompiled for x64 systems. 
This example will also work on x86, however all the projects will need to be recompiled for that platform. 
Also note: using x86 limits the size of the mesh you load, attempting this on an x86 will restrict your memory to around 1GB.
When building 3D models/meshes using 3D modelling software, set the camera to a location of x=0, y=0, and z=2.0 to represent where the HoloLens for this application positions the Holograms.
Also, you may need to scale (down or up) the models for proper viewing.
In my testing, I've found the .ply (best), .obj, .fbx formats work the best. The Models are loaded with the Open Asset importer open source library. 
While this library supports practically all the 3D model formats, for some reason the best formats are the models where all the meshes are grouped as 1 object as opposed to smaller individual objects.
Also not all model formats keep the materials and textures in the same file, and in this case the materials and textures don't load properly.
Another note, when running on a UWP application, some of the formats are compressed and require an unzipping, the Open asset importer doesn't know about the UWP file access architecture and thus throws an error for these file formats.
As far as positioning, the .ply format keeps the translation in it's format, along with textures and materials. I've found this format to be the best, however also one of the biggest file sizes, and thus also one of the longest to load and stream to the HoloLens.
![](https://i.imgur.com/q48IzLq.jpg)
